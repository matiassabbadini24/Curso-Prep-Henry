// En estas primeras 6 preguntas, reemplaza `null` por la respuesta

// Crea una variable "string", puede contener lo que quieras:
const nuevaString = 'Hello';


// Crea una variable numérica, puede ser cualquier número:
const nuevoNum = 30;


// Crea una variable booleana:
const nuevoBool = true;


// Resuelve el siguiente problema matemático:
const nuevaResta = 10 - 5 === 5;


// Resuelve el siguiente problema matemático:
const nuevaMultiplicacion = 10 * 4 === 40;


// Resuelve el siguiente problema matemático:
const nuevoModulo = 21 % 5 === 1;



// En los próximos 22 problemas, deberás completar la función.
// Todo tu código irá dentro de las llaves de la función.
// Asegúrate que usas "return" cuando la consola te lo pida.
// Pista: "console.log()" NO fucionará.
// No cambies los nombres de las funciones.

// "Return" la string provista: str
// Tu código: var string

function devolverString(str) {
  var string = str;
  return string;
}
devolverString('Hola Mundo');

// "x" e "y" son números
// Suma "x" e "y" juntos y devuelve el valor
// Tu código:Return x + y;

function suma(x, y) {
  return x + y;
}
suma(4, 6)

// Resta "y" de "x" y devuelve el valor
// Tu código:Return x - y;

function resta(x, y) {
  return x - y;
}
resta(10, 50)


// Multiplica "x" por "y" y devuelve el valor
// Tu código:Return x * y;

function multiplica(x, y) {
  return x * y;
}
multiplica(5, 4)


// Divide "x" entre "y" y devuelve el valor
// Tu código:Return x / y;

function divide(x, y) {
  return x / y;
}
divide(20, 5)


// Devuelve "true" si "x" e "y" son iguales
// De lo contrario, devuelve "false"
// Tu código:Return false;

function sonIguales(x, y) {
  if(x === y) {
  return true
} else return false
}
sonIguales(5, 5)

// Devuelve "true" si las dos strings tienen la misma longitud
// De lo contrario, devuelve "false"
// Tu código:Return false;

function tienenMismaLongitud(str1, str2) {
  return str1 === str2
}
tienenMismaLongitud(6, 6)


// Devuelve "true" si el argumento de la función "num" es menor que noventa
// De lo contrario, devuelve "false"
// Tu código:Return true;


function menosQueNoventa(num) {
  var num = num;
  if (num < 90) {
    return true
  } else { return false }
}
menosQueNoventa(100)


// Devuelve "true" si el argumento de la función "num" es mayor que cincuenta
// De lo contrario, devuelve "false"
// Tu código:Return true;
function mayorQueCincuenta(num) {
  var num = num;
  if (num > 50) {
    return true
  } else { return false }
}
mayorQueCincuenta(35);


// Obten el resto de la división de "x" entre "y"
// Tu código:x % y = 1

function obtenerResto(x, y) {
  return x % y == 1;
}
obtenerResto(4, 8);


// Devuelve "true" si "num" es par
// De lo contrario, devuelve "false"
// Tu código:Return false;
function esPar(num) {
  if (num % 2 == 0) {
    return true
  }
  else {
    return false
  }
}
esPar(6);


// Devuelve "true" si "num" es impar
// De lo contrario, devuelve "false"
// Tu código:Return true;

function esImpar(num) {
  if (num % 2 == 1) {
    return true
  }
  else {
    return false
  }
}
esImpar(9);


// Devuelve el valor de "num" elevado al cuadrado
// ojo: No es raiz cuadrada!
// Tu código:mathpow(5,2) = 25;

function elevarAlCuadrado(num) {
  return num = Math.pow(5, 2);
}
elevarAlCuadrado();

// Devuelve el valor de "num" elevado al cubo
// Tu código:mathpow(2,3) = 8;
function elevarAlCubo(num) {
  return num = Math.pow(2, 3)
}
elevarAlCubo();

// Devuelve el valor de "num" elevado al exponente dado en "exponent"
// Tu código:mathpow(2,4) = 16;
function elevar(num, exponent) {
  return num = Math.pow(2, 4)
}
elevar();

// Redondea "num" al entero más próximo y devuélvelo
// Tu código:mathround(7,5) = 8;
function redondearNumero(num) {
  return num = Math.round(7.5);
}
redondearNumero();

// Redondea "num" hacia arriba (al próximo entero) y devuélvelo
// Tu código:mathceil(7.0003) = 8;
function redondearHaciaArriba(num) {
  return num = Math.ceil(num);
}
redondearHaciaArriba(0.700);


//Generar un número al azar entre 0 y 1 y devolverlo
//Pista: investigá qué hace el método Math.random()
function numeroRandom() {
  return num = Math.floor(Math.random() * 100); + 1;
}
numeroRandom(6);

//La función va a recibir un entero. Devuelve como resultado una cadena de texto que indica si el número es positivo o negativo. 
//Si el número es positivo, devolver ---> "Es positivo"
//Si el número es negativo, devolver ---> "Es negativo"
//Si el número es 0, devuelve false
function esPositivo(numero) {
  return numero > 0 ? "Es Positivo" : "Es Negativo";
}
esPositivo(-1);

// Agrega un símbolo de exclamación al final de la string "str" y devuelve una nueva string
// Ejemplo: "hello world" pasaría a ser "hello world!"
// Tu código:
function agregarSimboloExclamacion(str) {
  return str + "!"
}
agregarSimboloExclamacion('Hello World');

// Devuelve "nombre" y "apellido" combinados en una string y separados por un espacio.
// Ejemplo: "Soy", "Henry" -> "Soy Henry"
// Tu código:
function combinarNombres(nombre, apellido) {
  return nombre + ' ' + apellido;
};
combinarNombres('Soy', 'Henry');


// Toma la string "nombre" y concatena otras string en la cadena para que tome la siguiente forma:
// "Martin" -> "Hola Martin!"
// Tu código:
function obtenerSaludo(nombre) {
  let saludar = 'Hola';
  nombre = 'Martin';
  return saludar + ' ' + nombre + "!"
};
obtenerSaludo();

// Retornar el area de un rectángulo teniendo su altura y ancho
// Tu código:
function obtenerAreaRectangulo(alto, ancho) {
  return alto * ancho;
}
obtenerAreaRectangulo(3, 6);


//Escibe una función a la cual reciba el valor del lado de un cuadrado y retorne su perímetro.
//Escribe tu código aquí
function retornarPerimetro(lado) {
  let perimetro = 4 * lado;
  return perimetro
}

retornarPerimetro(3);


//Desarrolle una función que calcule el área de un triángulo.
//Escribe tu código aquí

function areaDelTriangulo(base, altura) {
  return base * altura / 2;
}
areaDelTriangulo(4, 6)


//Supongamos que 1 euro equivale a 1.20 dólares. Escribe un programa que reciba
//como parámetro un número de euros y calcule el cambio en dólares.
//Escribe tu código aquí
function deEuroAdolar(euro) {
  var dolar = 1.20;
  var convercion = euro * dolar;
  return convercion
} deEuroAdolar();
deEuroAdolar(2);



//Escribe una función que reciba una letra y, si es una vocal, muestre el mensaje “Es vocal”. 
//Verificar si el usuario ingresó un string de más de un carácter y, en ese caso, informarle 
//que no se puede procesar el dato mediante el mensaje "Dato incorrecto".
//Escribe tu código aquí

function esVocal(letra) {
  letra = 'a';
  if (letra.match(/[aeiou]/gi)) {
    return letra + " Es Vocal"
  } else (letra !== letra.match(/[bcdgftjklyzlwrsnmqx]/gi))
  return letra + " No es vocal " + "Dato incorrecto"
}
esVocal();




// No modificar nada debajo de esta línea
// --------------------------------

module.exports = {
  nuevaString,
  nuevoNum,
  nuevoBool,
  nuevaResta,
  nuevaMultiplicacion,
  nuevoModulo,
  devolverString,
  tienenMismaLongitud,
  sonIguales,
  menosQueNoventa,
  mayorQueCincuenta,
  suma,
  resta,
  divide,
  multiplica,
  obtenerResto,
  esPar,
  esImpar,
  elevarAlCuadrado,
  elevarAlCubo,
  elevar,
  redondearNumero,
  redondearHaciaArriba,
  numeroRandom,
  esPositivo,
  agregarSimboloExclamacion,
  combinarNombres,
  obtenerSaludo,
  obtenerAreaRectangulo,
  retornarPerimetro,
  areaDelTriangulo,
  deEuroAdolar,
  esVocal,
};
