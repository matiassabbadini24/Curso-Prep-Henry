// No cambies los nombres de las funciones.

// "x" e "y" son números enteros (int).
// Devuelve el número más grande
// Si son iguales, devuelve cualquiera de los dos
// Tu código:
function obtenerMayor(x, y) {
    if (x < y) {
        return console.log((x + " es menor que " + y))
    } else (x > y)
    return console.log((x + " es mayor que " + y))
}

obtenerMayor(6, 5);


//Determinar si la persona según su edad puede ingresar a un evento.
//Si tiene 18 años ó más, devolver --> "Allowed"
//Si es menor, devolver --> "Not allowed"
function mayoriaDeEdad(edad) {
    if (edad >= 18) {
        return console.log(("Allowed"))
    } else return console.log(("Not Allowed"))
}
mayoriaDeEdad(18);


//Recibimos un estado de conexión de un usuario representado por un valor numérico. 
//Cuando el estado es igual a 1, el usuario está "Online"
//Cuando el estado es igual a 2, el usuario está "Away"
//De lo contrario, presumimos que el usuario está "Offline"
//Devolver el estado de conexión de usuario en cada uno de los casos.
function conection(status) {
    if (status == 1) {
        return console.log(("El usuario esta " + "Online"));
    } else if (status == 2) {
        return console.log(("El usuario esta " + "Away"));
    } else return console.log(("El usuario esta " + "Offline"));
}
conection(1);



// Devuelve un saludo en tres diferentes lenguajes:
// Si "idioma" es "aleman", devuelve "Guten Tag!"
// Si "idioma" es "mandarin", devuelve "Ni Hao!"
// Si "idioma" es "ingles", devuelve "Hello!"
// Si "idioma" no es ninguno de los anteiores o es `undefined` devuelve "Hola!"
// Tu código:

function saludo(idioma) {
    idioma;
    switch (idioma) {
        case "aleman":
            console.log("Guthen Tag!")
            break;
        case "mandarin":
            console.log("Ni Hao!")
            break;
        case "ingles":
            console.log("Hello!")
            break;
        default: console.log("Hola!")
            break;
    }
}
saludo("ingles")




//La función recibe un color. Devolver el string correspondiente:
//En caso que el color recibido sea "blue", devuleve --> "This is blue"
//En caso que el color recibido sea "red", devuleve --> "This is red"
//En caso que el color recibido sea "green", devuleve --> "This is green"
//En caso que el color recibido sea "orange", devuleve --> "This is orange"
//Caso default: devuelve --> "Color not found"
//Usar el statement Switch.

function colors(color) {
    switch (color) {
        case "blue":
            console.log("This is blue")
            break;
        case "red":
            console.log("This is red")
            break;
        case "green":
            console.log("This is green")
            break;
        case "orange":
            console.log("This is orange")
            break;
        default: console.log("Color not found")
            break;
    }
}
colors("red")

// Devuelve "true" si "numero" es 10 o 5
// De lo contrario, devuelve "false"
// Tu código:

function esDiezOCinco(numero) {
    if (numero === 5 || numero == 10) {
        return console.log(true)
    } else (numero !== 10 || numero !== 5)
    return console.log(false)
}
esDiezOCinco(10);


// Devuelve "true" si "numero" es menor que 50 y mayor que 20
// De lo contrario, devuelve "false"
// Tu código:

function estaEnRango(numero) {
    if (numero >= 20 && numero <= 50) {
        return console.log(true)
    } else if (numero <= 19 || numero >= 51) {
        return console.log(false)
    }
}
estaEnRango(60);



// Devuelve "true" si "numero" es un entero (int/integer)
// Ejemplo: 0.8 -> false
// Ejemplo: 1 -> true
// Ejemplo: -10 -> true
// De lo contrario, devuelve "false"
// Pista: Puedes resolver esto usando `Math.floor`
// Tu código:

function esEntero(numero) {
    return console.log((~~numero === numero));
}
esEntero(6);





// Si "numero" es divisible entre 3, devuelve "fizz"
// Si "numero" es divisible entre 5, devuelve "buzz"
// Si "numero" es divisible entre 3 y 5 (ambos), devuelve "fizzbuzz"
// De lo contrario, devuelve el numero

function fizzBuzz(numero) {
    if (numero % 3 == 0) {
        return console.log("fizz")
    } else if (numero % 5 == 0)
        return console.log("buzz")
    else (numero % 5 == 0 && numero % 3 == 0)
    return console.log("fizzbuzz")
}
fizzBuzz(25)




//La función recibe tres números distintos. 
//Si num1 es mayor a num2 y a num3 y además es positivo, retornar ---> "Número 1 es mayor y positivo"
//Si alguno de los tres números es negativo, retornar ---> "Hay negativos"
//Si num3 es más grande que num1 y num2, aumentar su valor en 1 y retornar el nuevo valor.
//0 no es ni positivo ni negativo. Si alguno de los argumentos es 0, retornar "Error".
//Si no se cumplen ninguna de las condiciones anteriores, retornar false. 

function operadoresLogicos(num1, num2, num3) {
    if (num1 > 0 && num1 > num2 && num1 > num3) {
        return console.log("Número 1 es mayor y positivo")
    } else if (num1 < 0 && num2 < 0 && num3 < 0) {
        return console.log("Hay negativos")
    } else if (num3 > num1 && num3 > num2) {
        console.log(num3 + 1);
    } else if (num1 === 0 && num2 === 0 && num3 === 0) {
        return console.log("Error")
    } else return console.log(false)
}
operadoresLogicos(0, 3, 4);




// Devuelve "true" si "numero" es primo
// De lo contrario devuelve "falso"
// Pista: un número primo solo es divisible por sí mismo y por 1
// Pista 2: Puedes resolverlo usando un bucle `for`
// Nota: Los números 0 y 1 NO son considerados números primos

function esPrimo(numero) {
    for (var i = 2; i < numero; i++) {
        if (numero % i === 0) {
            return console.log(false);
        }
    }
    if (numero === 1) {
    } else 
        return console.log(true)
}
esPrimo(7);




//Escribe una función que reciba un valor booleano y retorne “Soy verdadero” 
//si su valor es true y “Soy falso” si su valor es false.
//Escribe tu código aquí


function esVerdadero(valor) {
    if (valor === true) {
        return "Soy Verdadero"
    } else (valor === false)
    return "Soy Falso"

} esVerdadero(true);

//Escribe una función que muestre la tabla de multiplicar del 6 (del 0 al 60).
//La función devuelve un array con los resultados de la tabla de multiplicar del 6 en orden creciente.
//Escribe tu código aquí   

function tablaDelSeis() {
    tabla = 6;
    for (let index = 1; index <= 10; index++) {
        let multiplicar = index * tabla;
        console.log(multiplicar)
    }
}
tablaDelSeis();



//Leer un número entero y retornar true si tiene 3 dígitos. Caso contrario, retorna false.
//Escribe tu código aquí

function tieneTresDigitos(numero) {
    if (numero < 99) {
        return false
    } else (numero > 100)
    return true
}
tieneTresDigitos(123);



//Implementar una función tal que vaya aumentando el valor recibido en 5 hasta un límite de 8 veces
//Retornar el valor final.
//Usar el bucle do ... while.

function doWhile(numero) {
    do {
        numero++
        console.log(numero)
    } while (numero < 13);
}
doWhile(5);



// No modificar nada debajo de esta línea
// --------------------------------

module.exports = {
    obtenerMayor,
    mayoriaDeEdad,
    conection,
    saludo,
    colors,
    esDiezOCinco,
    estaEnRango,
    esEntero,
    fizzBuzz,
    operadoresLogicos,
    esPrimo,
    esVerdadero,
    tablaDelSeis,
    tieneTresDigitos,
    doWhile
};
